extends KinematicBody

var gravity = -9.8
var velocity = Vector3()
var camera
var anim_player
var character
var en_onion = "no";

var pikmin_rojo_escena = load("res://models/Pikmin/pikminred.tscn");
var pikmin_azul_escena = load("res://models/Pikmin/pikminblue.tscn");
var pikmin_amarillo_escena = load("res://models/Pikmin/piki_p3_yellow.tscn");
var pikmin_rojo = pikmin_rojo_escena.instance();
var pikmin_azul = pikmin_azul_escena.instance();
var pikmin_amarillo = pikmin_amarillo_escena.instance();
var nivel = get_parent();
onready var whistle = get_node("Reticle/Whistle");
onready var whistle_shape = get_node("Reticle/Whistle/Shape");
onready var whistle_mesh = get_node("Reticle/Whistle/Shape/Mesh");
onready var mother = get_node("Mother");
onready var reticle = get_node("Reticle")
onready var onion_blue = get_parent().get_node("Blue Onion")
onready var onion_yellow = get_parent().get_node("Yellow Onion")
onready var onion_red = get_parent().get_node("Red Onion")

export var SPEED = 6
export var ACCELERATION = 3
export var DE_ACCELERATION = 5

var silbatando = true;
var move_sonido = true;

func _ready():
	anim_player = get_node("AnimationPlayer")
	character = get_node(".")
	nivel = get_parent();
	$walking.stream_paused = true;
	$walking.play()
	$Whistle.stream_paused = true;
	$Whistle.play()

func _physics_process(delta):
	
	camera = get_node("target/Camera").get_global_transform()
	var dir = Vector3()
	
	var is_moving = false

	if(Input.is_action_pressed("move_fw")):
		dir += -camera.basis[2]
		is_moving = true
	if(Input.is_action_pressed("move_bw")):
		dir += camera.basis[2]
		is_moving = true
	if(Input.is_action_pressed("move_l")):
		dir += -camera.basis[0]
		is_moving = true
	if(Input.is_action_pressed("move_r")):
		dir -= -camera.basis[0]
		is_moving = true
		
	dir.y = 0
	dir = dir.normalized()
	
	velocity.y += delta * gravity
	
	var hv = velocity
	hv.y = 0
	
	var new_pos = dir * SPEED
	var accel = DE_ACCELERATION
	
	if (dir.dot(hv) > 0):
		accel = ACCELERATION
		
	hv = hv.linear_interpolate(new_pos, accel * delta)
	
	velocity.x = hv.x
	velocity.z = hv.z
			
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))	
	
	if is_moving:
		
		$walking.stream_paused = false;
		
		var angle = atan2(hv.x, hv.z)
		
		var char_rot = character.get_rotation()
		
		char_rot.y = angle
		character.set_rotation(char_rot)
	else :
		$walking.stream_paused = true;
	
	var speed = hv.length() / SPEED
	
	var horizontal = Input.get_action_strength("mother_l") - Input.get_action_strength("mother_r")
	var vertical = Input.get_action_strength("mother_u") - Input.get_action_strength("mother_d")
	
	if (Input.is_action_pressed("ui_cancel")):
		
		whistle_activate();
	if (Input.is_action_just_released("ui_cancel")):
		whistle_deactivate()

	if (mother.translation.x >= -0.536244 and mother.translation.x <= 0.536244 and mother.translation.z >= -1 and mother.translation.z <= 1):
		mother.translate(Vector3(0.1 * horizontal, 0.0, 0.1 * vertical))
	if (horizontal == 0) :
		mother.translation.x = 0
	
	if (vertical == 0) :
		mother.translation.z = -0.56541


func _on_amarillo_body_entered(body):
	en_onion = "amarillo";

func _on_amarillo_body_exited(body):
	en_onion = "no";

func _on_rojo_body_entered(body):
	en_onion = "rojo";

func _on_rojo_body_exited(body):
	en_onion = "no";

func _on_azul_body_entered(body):
	en_onion = "azul";
	
func _on_azul_body_exited(body):
	en_onion = "no";

func _input(event):
	if (event.is_action_pressed("ui_accept")):
		get_pikmin()
			
	if (event.is_action_pressed("ui_accept")):
		throw()
	
	if (event.is_action_pressed("ui_cancel")):
		if silbatando :
			$Whistle.play()
	
	if (event.is_action_pressed("dismiss")):
		dismiss()
	
	if (event.is_action_pressed("zoom_out")):
		$Camera.play()
		$target/Camera.distancia +=0.5
	if (event.is_action_pressed("zoom_in")):
		$Camera.play()
		$target/Camera.distancia -=0.5

func whistle_activate () :
	$Whistle.stream_paused = false;
	whistle_mesh.show()
	whistle_shape.disabled = false;
	if (whistle_shape.scale < Vector3(16, 16, 16)):
		whistle_shape.scale += Vector3(0.8, 0.8, 0.8)
		whistle_shape.translate(Vector3 (0, 0, 0.1))

func dismiss ():
	if (mother.get_child_count() > 0):
		$Dismiss.play()
		var pikmins = [];
		pikmins = mother.get_children()
		for i in range (pikmins.size()):
			var pikmin_position = []
			$PikminAh.play()
			pikmin_position = pikmins[i].get_global_transform()
			mother.remove_child(pikmins[i])
			nivel.add_child(pikmins[i])
			pikmins[i].set_owner(nivel)
			pikmins[i].set_global_transform(pikmin_position)
		

func throw ():
	if(en_onion == "no" and mother.get_child_count() > 0):
		$PikminThrow.play()
		var pikmins = [];
		pikmins = mother.get_children()
		mother.remove_child(pikmins[0])
		nivel.add_child(pikmins[0])
		pikmins[0].set_owner(nivel)
		var reticle_position =[]
		reticle_position= reticle.get_global_transform()
		var pikmin_position = []
		pikmin_position = pikmins[0].get_global_transform()
		pikmin_position[3] = reticle_position[3]
		var anim = pikmins[0].get_node('AnimationPlayer')
		anim.play('pikminspin')
		pikmins[0].set_global_transform(pikmin_position)

func get_pikmin ():
	if (en_onion == "azul"):
		$OnionPop.play()
		var pikmin_azul = pikmin_azul_escena.instance();
		pikmin_azul.translation = onion_blue.translation + Vector3(rand_range(-1,1), 0 , rand_range(-1,1))
		nivel.add_child(pikmin_azul)
		$PikminWo.play()
	elif (en_onion == "rojo"):
		$OnionPop.play()
		var pikmin_rojo = pikmin_rojo_escena.instance();
		pikmin_rojo.translation = onion_red.translation + Vector3(rand_range(-1,1), 0 , rand_range(-1,1))
		nivel.add_child(pikmin_rojo)
		$PikminWo.play()
	elif (en_onion == "amarillo"):
		$PikminWo.play()
		$OnionPop.play()
		var pikmin_amarillo = pikmin_amarillo_escena.instance();
		pikmin_amarillo.translation = onion_yellow.translation + Vector3(rand_range(-1,1), 0 , rand_range(-1,1))
		nivel.add_child(pikmin_amarillo)
		$PikminWo.play()

func whistle_deactivate () :
	$Whistle.stream_paused = true;
	whistle_mesh.hide()
	whistle_shape.disabled = true;
	whistle_shape.scale = Vector3(1, 1, 1)
	whistle_shape.translation = Vector3(0, 0, 0)

func _on_Whistle_body_entered(body):
	if check_children(body):
		$PikminCall.play()
		reparent(body)

func reparent (nodo) :
	nivel.remove_child(nodo)
	nodo.get_node("Tween").interpolate_property(nodo,"translation",nodo.translation, mother.translation + Vector3(rand_range(-0.8,0.8), 0 , rand_range(-0.2, 0.4)), 0.5,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	nodo.get_node("Tween").start()
	nodo.translation = mother.translation + Vector3(rand_range(-0.8,0.8), 0 , rand_range(-0.3,0.3))
	mother.add_child(nodo)

func check_children (nodo):
	var hijos = []
	hijos = mother.get_children()
	if(hijos.size() == 0):
		return true
	for i in range (hijos.size()):
		if (nodo == hijos[i]):
			return false
		else:
			return true

func _on_Whistle_finished():
	silbatando = false;
	pass # Replace with function body.
