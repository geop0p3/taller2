extends "pawn.gd"

onready var Grid = get_parent()
var input_direction

func _ready():
	update_look_direction(Vector2(1, 0))

func move():
	input_direction = get_input_direction()
	if not input_direction:
		return
	update_look_direction(input_direction)

	var target_position = Grid.request_move_monster(self, input_direction)
	if target_position:
		move_to(target_position)
	else:
		bump()

func get_input_direction():
	return Vector2(
		int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left")),
		int(Input.is_action_pressed("ui_up")) -int(Input.is_action_pressed("ui_down"))
	)

func update_look_direction(direction):
	$Pivot/Sprite.rotation = direction.angle()
	if direction.angle() > 3 :
		$Pivot/Sprite.set_flip_v(true)
	else:
		$Pivot/Sprite.set_flip_v(false)

func move_to(target_position):
	var move_direction = (target_position - position).normalized()
	$Tween.interpolate_property($Pivot/Sprite, "position", - move_direction * 16, Vector2(), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	position = target_position
	
func death():
	set_process(false)
	var sound = get_node("sound")
	sound.play()
	$Pivot/Sprite.animation="Death"
	var i = Grid.Monsters.find(self)
	yield(get_tree().create_timer(0.3), "timeout")
	Grid.Monsters.remove(i)
	queue_free()
	set_process(true)

func bump():
	pass