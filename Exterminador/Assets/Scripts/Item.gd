extends "pawn.gd"

func playsound():
	print("Playing Sound")
	var sound = get_node("play")
	sound.play()
	$Pivot/Sprite.hide()
	yield(get_tree().create_timer(0.4), "timeout")
	self.queue_free()
