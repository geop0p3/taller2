extends "pawn.gd"

onready var Grid = get_parent()
var input_direction


func move(direccion):
	update_look_direction(direccion)

	var target_position = Grid.request_move_bullet(self, input_direction)
	if target_position:
		move_to(target_position)
	else:
		bump()

func update_look_direction(direction):
	$Pivot/Sprite.rotation = direction.angle()
	if direction.angle() > 3 :
		$Pivot/Sprite.set_flip_v(true)
	else:
		$Pivot/Sprite.set_flip_v(false)

func move_to(target_position):
	var move_direction = (target_position - position).normalized()
	$Tween.interpolate_property($Pivot/Sprite, "position", - move_direction * 16, Vector2(), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	position = target_position
	
func setdireccion(direccion):
	input_direction = direccion

func death():
	set_process(false)
	var i = Grid.Bullets.find(self)
	yield(get_tree().create_timer(0.3), "timeout")
	Grid.Bullets.remove(i)
	queue_free()
	set_process(true)

func bump():
	yield(get_tree().create_timer(0.3), "timeout")