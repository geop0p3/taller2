extends TileMap

enum CELL_TYPES { EMPTY = -1, ACTOR, OBSTACLE, OBJECT, SWITCH, BULLET, MONSTER }
onready var Monsters = []
onready var Bullets = []
onready var balaescena = preload("res://Scenes/Bullet.tscn")
func _ready():
	for node in get_children():
			set_cellv(world_to_map(node.position), node.type)
			if node.type == CELL_TYPES.MONSTER:
				Monsters.append(node)
			

func get_cell_pawn(coordinates):
	for node in get_children():
		if world_to_map(node.position) == coordinates:
			return(node)

func request_move(pawn, direction):
	
	var cell_start = world_to_map(pawn.position)
	var cell_target = cell_start + direction
	
	var cell_target_type = get_cellv(cell_target)
	match cell_target_type:
		CELL_TYPES.EMPTY:
			monstermove()
			if(Bullets):
				bulletmove()
			return update_pawn_position(pawn, cell_start, cell_target)
		CELL_TYPES.OBJECT:
			var object_pawn = get_cell_pawn(cell_target)
			object_pawn.playsound()
			variablesjugador.ammo +=1
			monstermove()
			if(Bullets):
				bulletmove()
			return update_pawn_position(pawn, cell_start, cell_target)
		CELL_TYPES.OBSTACLE:
			pass
		CELL_TYPES.ACTOR:
			pass
		CELL_TYPES.SWITCH:
			pass
		CELL_TYPES.MONSTER:
			if(Bullets):
				bulletmove()
			variablesjugador.death = true
		CELL_TYPES.BULLET:
			print("Actor on bullet")

func request_move_monster(pawn, direction):
	var cell_start = world_to_map(pawn.position)
	var cell_target = cell_start + direction
	
	var cell_target_type = get_cellv(cell_target)
	match cell_target_type:
		CELL_TYPES.EMPTY:
			return update_pawn_position(pawn, cell_start, cell_target)
		CELL_TYPES.OBJECT:
			print("monster on object")
		CELL_TYPES.ACTOR:
			print("game over")
		CELL_TYPES.BULLET:
			print("Monster on bullet")
			var m = Monsters.find(pawn)
			Monsters[m].death()
			var object_pawn = get_cell_pawn(cell_target)
			var i = Bullets.find(object_pawn)
			Bullets[i].death()
			set_cellv(cell_target, CELL_TYPES.EMPTY)
			set_cellv(cell_start, CELL_TYPES.EMPTY)
			return (map_to_world(cell_target) + cell_size / 2)

func request_move_bullet(pawn, direction):
	var cell_start = world_to_map(pawn.position)
	var cell_target = cell_start + direction
	
	var cell_target_type = get_cellv(cell_target)
	match cell_target_type:
		CELL_TYPES.EMPTY:
			var cell_start_type = get_cellv(cell_start)
			if(cell_start_type == 2):
				return (map_to_world(cell_target) + cell_size / 2)
			else:
				return update_pawn_position(pawn, cell_start, cell_target)
		CELL_TYPES.OBJECT:
			set_cellv(cell_start, CELL_TYPES.EMPTY)
			return (map_to_world(cell_target) + cell_size / 2)
		CELL_TYPES.OBSTACLE:
			print("Bullet on Obstacle")
			var i = Bullets.find(pawn)
			Bullets[i].death()
			set_cellv(cell_start, CELL_TYPES.EMPTY)
		CELL_TYPES.ACTOR:
			print("Bullet on Player")
		CELL_TYPES.SWITCH:
			var i = Bullets.find(pawn)
			Bullets[i].death()
			set_cellv(cell_start, CELL_TYPES.EMPTY)
		CELL_TYPES.MONSTER:
			var object_pawn = get_cell_pawn(cell_target)
			var m = Monsters.find(object_pawn)
			Monsters[m].death()
			var i = Bullets.find(pawn)
			Bullets[i].death()
			set_cellv(cell_target, CELL_TYPES.EMPTY)
			set_cellv(cell_start, CELL_TYPES.EMPTY)
			return (map_to_world(cell_target) + cell_size / 2)

func update_pawn_position(pawn, cell_start, cell_target):
	set_cellv(cell_target, pawn.type)
	set_cellv(cell_start, CELL_TYPES.EMPTY) # Esta linea hace el problema, al pasar por un objeto se limpia su tipo.
	return map_to_world(cell_target) + cell_size / 2

func shoot(pawn, direction):
	print("No Ammo")
	if (variablesjugador.ammo > 0):
		print("Disparando")
		var cell_start = world_to_map(pawn.position)
		var cell_target = cell_start + direction
		var posicion = map_to_world(cell_target) + cell_size / 2
		variablesjugador.ammo -= 1
		var bala = balaescena.instance()
		bala.position=posicion
		bala.setdireccion(direction)
		Bullets.append(bala)
		add_child(bala)
		print(Bullets)
		
func monstermove():
	if(Monsters):
		for i in range (Monsters.size()):
			Monsters[i].move()
			
func bulletmove():
	if(Bullets):
		for i in range (Bullets.size()):
			var cell_start = world_to_map(Bullets[i].position)
			var cell_target = cell_start + Bullets[i].input_direction
			var destination = map_to_world(cell_target) + cell_size / 2
			Bullets[i].move(destination)
			