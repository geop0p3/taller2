extends "pawn.gd"

onready var Grid = get_parent()
var input_direction
var previouslook = Vector2(1,0)

func _ready():
	update_look_direction(Vector2(1, 0))

func _process(delta):
	input_direction = get_input_direction()
	if not input_direction:
		return
	update_look_direction(input_direction)

	var target_position = Grid.request_move(self, input_direction)
	if target_position:
		move_to(target_position)
	else:
		bump()

func get_input_direction():
	return Vector2(
		int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left")),
		int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	)

func update_look_direction(direction):
	$Pivot/Sprite.rotation = direction.angle()
	if direction.angle() > 3 :
		$Pivot/Sprite.set_flip_v(true)
	else:
		$Pivot/Sprite.set_flip_v(false)

func move_to(target_position):
	set_process(false)

	var move_direction = (target_position - position).normalized()
	$Tween.interpolate_property($Pivot, "position", - move_direction * 16, Vector2(1,0), 0.2, Tween.TRANS_SINE, Tween.EASE_IN)
	$Pivot/Sprite.animation="Run"
	
	$Tween.start()
	position = target_position
	var sound = get_node("sound")
	sound.play()

	yield(get_tree().create_timer(0.2), "timeout")
	$Pivot/Sprite.animation="Idle"
	
	set_process(true)

func _input(event):
	if Input.is_action_just_released("ui_right"):
		previouslook = Vector2(1,0)
	
	if Input.is_action_just_released("ui_left"):
		previouslook = Vector2(-1,0)
		
	if Input.is_action_just_released("ui_up"):
		previouslook = Vector2(0,-1)
	
	if Input.is_action_just_released("ui_down"):
		previouslook = Vector2(0,1)
	
	if Input.is_action_pressed("shoot"):
		Grid.shoot(self, previouslook)


func bump():
	if(variablesjugador.death == true):
		set_process(false)
		var sound = get_node("death")
		sound.play()
		$Pivot/Sprite.animation="Death"
		yield(get_tree().create_timer(1.4), "timeout")
		get_tree().quit()
	else:
		set_process(false)
		$Pivot/Sprite.animation="Run"
		yield(get_tree().create_timer(0.3), "timeout")
		$Pivot/Sprite.animation="Idle"
		set_process(true)